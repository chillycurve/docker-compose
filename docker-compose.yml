version: "3.7"
services:

## Backend
  traefik:
      # Touch (create empty files) traefik.log and acme/acme.json. Set acme.json permissions to 600.
      # touch $USERDIR/docker/traefik2/acme/acme.json
      # chmod 600 $USERDIR/docker/traefik2/acme/acme.json
      # touch $USERDIR/docker/traefik2/traefik.log
    image: traefik:v2.2
    ports:
      - 80:80
      - 443:443
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - $USERDIR/docker/traefik/letsencrypt:/letsencrypt
      - $USERDIR/docker/traefik/dynamic/:/dynamic/
      - $USERDIR/docker/traefik/static/:/etc/traefik/
      - $USERDIR/docker/traefik/log:/log
    command:
      - --providers.docker
      - --entrypoints.web.address=:80
      - --entrypoints.websecure.address=:443
      - --entrypoints.web.http.redirections.entryPoint.to=websecure
      - --entrypoints.https.forwardedHeaders.trustedIPs=173.245.48.0/20,103.21.244.0/22,103.22.200.0/22,103.31.4.0/22,141.101.64.0/18,108.162.192.0/18,190.93.240.0/20,188.114.96.0/20,197.234.240.0/22,198.41.128.0/17,162.158.0.0/15,104.16.0.0/12,172.64.0.0/13,131.0.72.0/22
      - --entrypoints.web.http.redirections.entryPoint.scheme=https
      - --providers.docker.network=traefik
      - '--providers.docker.defaultRule=Host(`proxy.$DOMAINNAME`)'
      - "--certificatesresolvers.mydnschallenge.acme.dnschallenge=true"
      - "--certificatesresolvers.mydnschallenge.acme.dnschallenge.provider=cloudflare"
      - "--certificatesresolvers.mydnschallenge.acme.email=postmaster@example.com"
      - "--certificatesresolvers.mydnschallenge.acme.storage=/letsencrypt/acme.json"
      - "traefik.http.services.traefik-svc.loadbalancer.server.port=8080"
      - "traefik.http.routers.traefik-rtr.middlewares=keycloak@file"
      - "traefik.http.routers.api.service=api@internal"
    environment:
      - "CF_DNS_API_TOKEN=$CF_DNS_API_TOKEN"
      - "CF_ZONE_API_TOKEN=$CF_ZONE_API_TOKEN"
    networks:
      traefik:
        ipv4_address: $SERVERIP # You can specify a static IP

  keycloak:
    ## Keycloak - Private Authentication
    ## To fix net.core.*mem issues add these to the bottom of /etc/sysctl.conf
    ## https://forums.docker.com/t/how-to-tune-kernel-properties-in-docker-images/25291
    # ## Allow a 25MB UDP receive buffer for JGroups
    # net.core.rmem_max = 26214400
    # ## Allow a 1MB UDP send buffer for JGroups
    # net.core.wmem_max = 1048576
    # or
    # sudo sysctl -w net.core.rmem_max=26214400
    # sudo sysctl -w net.core.wmem_max=1048576
    ## https://www.cyberciti.biz/faq/reload-sysctl-conf-on-linux-using-sysctl/
    ## Suggested procedure to create your KeyCloak Database:
    ## Change the <password>
    # docker exec -it mariadb mysql -uroot -p$MYSQL_ROOT_PASSWORD
    # CREATE DATABASE keycloak CHARACTER SET = utf8 COLLATE = utf8_unicode_ci;
    # GRANT ALL PRIVILEGES ON keycloak.* TO 'keycloak'@'keycloak.mariadb' IDENTIFIED BY '<password>';
    # FLUSH PRIVILEGES;
    # exit
    ## Initialize admin account:
    # docker exec keycloak keycloak/bin/add-user-keycloak.sh -u <username> -p <password>
    container_name: keycloak
    image: jboss/keycloak:latest
    restart: always
    networks:
      - traefik
    depends_on:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      - /etc/:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    environment:
      - PUID=$PUID
      - PGID=$PGID
      - KEYCLOAK_USER=$MY_USERNAME
      - KEYCLOAK_PASSWORD=$MY_PASSWORD
      - DB_VENDOR=mariadb
      - DB_ADDR=mariadb
      - DB_DATABASE=keycloak
      - DB_USER=keycloak
      - DB_PASSWORD=$MYSQL_PASSWORD
      - PROXY_ADDRESS_FORWARDING=true
      - KEYCLOAK_HOSTNAME=auth.$DOMAINNAME
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.keycloak-rtr.entrypoints=https"
      - "traefik.http.routers.keycloak-rtr.rule=Host(`auth.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.keycloak-rtr.middlewares=secure-chain@file"
      ## HTTP Services
      - "traefik.http.routers.keycloak-rtr.service=keycloak-svc"
      - "traefik.http.services.keycloak-svc.loadbalancer.server.port=8080"

  guacd:
    image: guacamole/guacd
    container_name: guacd
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true

  mariadb:
    container_name: mariadb
    image: linuxserver/mariadb:latest
    restart: always
    networks:
      traefik
    security_opt:
      - no-new-privileges:true
    ports:
      - "3306:3306"
    volumes:
      - $USERDIR/docker/mariadb/data:/config
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    environment:
      - PUID=$PUID
      - PGID=$PGID
      - MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD

  influxdb:
    #create influxdb.conf
    image: influxdb
    container_name: influxdb
    restart: always
    security_opt:
      - no-new-privileges:true
    ports:
      - "$INFLUXDB_PORT:8086"
    volumes:
      - $USERDIR/docker/influxdb/influxdb.conf:/etc/influxdb/influxdb.conf:ro
      - $USERDIR/docker/influxdb/db:/var/lib/influxdb
    command: -config /etc/influxdb/influxdb.conf

  postgres:
    image: postgres
    container_name: postgres
    restart: always
    security_opt:
      - no-new-privileges:true
    volumes:
      - $USERDIR/docker/postgres:/var/lib/postgresql/data
    environment:
      POSTGRES_PASSWORD: $STATPING_DB_PASS
      POSTGRES_USER: $STATPING_DB_USER
      POSTGRES_DB: $STATPING_DB

  dockergc:
    image: clockworksoul/docker-gc-cron:latest
    container_name: docker-gc
    restart: unless-stopped
    network_mode: none
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - $USERDIR/docker/docker-gc/docker-gc-exclude:/etc/docker-gc-exclude
    environment:
      CRON: 0 0 * * *
      FORCE_IMAGE_REMOVAL: 1
      FORCE_CONTAINER_REMOVAL: 0
      GRACE_PERIOD_SECONDS: 604800
      DRY_RUN: 0
      CLEAN_UP_VOLUMES: 1
      TZ: $TZ

  matomo:
    image: matomo:fpm-alpine
    container_name: matomo
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      - $USERDIR/docker/matomo/var/www/html:/var/www/html
    environment:
      MATOMO_DATABASE_TABLES_PREFIX: MATAMO_
      MATOMO_DATABASE_USERNAME: $MYSQL_USER
      MATOMO_DATABASE_PASSWORD: $MYSQL_PASSWORD
      MATOMO_DATABASE_DBNAME: MATAMO
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.matomo-rtr.entrypoints=https"
      - "traefik.http.routers.matomo-rtr.rule=Host(`matomo.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.matomo-rtr.middlewares=keycloak@file"
      ## HTTP Services
      - "traefik.http.routers.matomo-rtr.service=matamo-svc"
      - "traefik.http.services.matomo-svc.loadbalancer.server.port=80"

## System Management
  guacamole:
    image: guacamole/guacamole:latest
    container_name: guacamole
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    environment:
      GUACD_HOSTNAME: guacd
      MYSQL_HOSTNAME: $DB_HOST
      MYSQL_PORT: $DB_PORT
      MYSQL_DATABASE: guacamole
      MYSQL_USER: $GUAC_MYSQL_USER
      MYSQL_PASSWORD: $GUAC_MYSQL_PASSWORD
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.guacamole-rtr.entrypoints=https"
      - "traefik.http.routers.guacamole-rtr.rule=Host(`guac.$DOMAINNAME`)"
      - "traefik.http.routers.guacamole-rtr.tls=true"
      ## Middlewares
      - "traefik.http.routers.guacamole-rtr.middlewares=keycloak@file,add-guacamole"
      - "traefik.http.middlewares.add-guacamole.addPrefix.prefix=/guacamole"
      ## HTTP Services
      - "traefik.http.routers.guacamole-rtr.service=guacamole-svc"
      - "traefik.http.services.guacamole-svc.loadbalancer.server.port=8080"

  phpmyadmin:
    image: phpmyadmin/phpmyadmin:latest
    container_name: phpmyadmin
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    # ports:
    #   - "$PHPMYADMIN_PORT:80"
    # volumes:
    #   - $USERDIR/docker/phpmyadmin:/etc/phpmyadmin
    environment:
      # - PMA_HOST=$DB_HOST
      - PMA_PORT=$DB_PORT
      - PMA_ARBITRARY=1
      - MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.phpmyadmin-rtr.entrypoints=https"
      - "traefik.http.routers.phpmyadmin-rtr.rule=Host(`pma.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.phpmyadmin-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.phpmyadmin-rtr.service=phpmyadmin-svc"
      - "traefik.http.services.phpmyadmin-svc.loadbalancer.server.port=80"

  portainer:
    container_name: portainer
    image: portainer/portainer:latest
    restart: unless-stopped
    command: -H unix:///var/run/docker.sock
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    # ports:
    #   - "$PORTAINER_PORT:9000"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - $USERDIR/docker/portainer/data:/data # Change to local directory if you want to save/transfer config locally
    environment:
      - TZ=$TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.portainer-rtr.entrypoints=https"
      - "traefik.http.routers.portainer-rtr.rule=Host(`portainer.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.portainer-rtr.middlewares=chain-authelia@file" # Authelia
      ## HTTP Services
      - "traefik.http.routers.portainer-rtr.service=portainer-svc"
      - "traefik.http.services.portainer-svc.loadbalancer.server.port=9000"

  grafana:
    image: grafana/grafana:latest
    container_name: grafana
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    # ports:
    #   - "$GRAFANA_PORT:3000"
    user: "0"
    volumes:
      - $USERDIR/docker/grafana:/var/lib/grafana
    environment:
      GF_INSTALL_PLUGINS: "grafana-clock-panel,grafana-simple-json-datasource,grafana-worldmap-panel,grafana-piechart-panel"
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.grafana-rtr.entrypoints=https"
      - "traefik.http.routers.grafana-rtr.rule=Host(`grafana.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.grafana-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.grafana-rtr.service=grafana-svc"
      - "traefik.http.services.grafana-svc.loadbalancer.server.port=3000"

  dozzle:
    image: amir20/dozzle:latest
    container_name: dozzle
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    # ports:
    #   - "$DOZZLE_PORT:8080"
    environment:
      DOZZLE_LEVEL: info
      DOZZLE_TAILSIZE: 300
      DOZZLE_FILTER: "status=running"
      # DOZZLE_FILTER: "label=log_me" # limits logs displayed to containers with this label
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.dozzle-rtr.entrypoints=https"
      - "traefik.http.routers.dozzle-rtr.rule=Host(`logs.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.dozzle-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.dozzle-rtr.service=dozzle-svc"
      - "traefik.http.services.dozzle-svc.loadbalancer.server.port=8080"

  embystat:
    image: linuxserver/embystat
    container_name: embystat
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=$TZ
    volumes:
      - $USERDIR/docker/embystat/config:/config
    restart: unless-stopped
    networks:
      - traefik
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.embystat-rtr.entrypoints=https"
      - "traefik.http.routers.embystat-rtr.rule=Host(`embystat.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.embystat-rtr.middlewares=chain-keycloak@file"
      ## HTTP Services
      - "traefik.http.routers.embystat-rtr.service=embystat-svc"
      - "traefik.http.services.embystat-svc.loadbalancer.server.port=3000"

## Matrix Backend
  matrix:
    container_name: matrix
    image: matrixdotorg/synapse:latest
    restart: always
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      - $USERDIR/docker/matrix/data:/data
    environment:
      - UID=$PUID
      - GID=$PGID
      - TZ=$TZ
      - SYNAPSE_SERVER_NAME=matrix.$DOMAINNAME
      - SYNAPSE_REPORT_STATS=yes
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.matrix-rtr.entrypoints=https"
      - "traefik.http.routers.matrix-rtr.rule=Host(`matrix.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.matrix-rtr.middlewares=secure-chain@file"
      ## HTTP Services
      - "traefik.http.routers.matrix-rtr.service=matrix-svc"
      - "traefik.http.services.matrix-svc.loadbalancer.server.port=8008"

  ma1sd:
    container_name: ma1sd
    image: ma1uta/ma1sd:latest
    restart: always
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      - $USERDIR/docker/matrix/ma1sd/etc:/etc/ma1sd
      - $USERDIR/docker/matrix/ma1sd/var:/var/ma1sd
    environment:
      - UID=$PUID
      - GID=$PGID
      - TZ=$TZ
      - MATRIX_DOMAIN=matrix.$DOMAINNAME
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.matrix-rtr.entrypoints=https"
      - "traefik.http.routers.matrix-rtr.rule=Host(`matrix-ident.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.matrix-rtr.middlewares=secure-chain@file"
      ## HTTP Services
      - "traefik.http.routers.matrix-rtr.service=matrix-svc"
      - "traefik.http.services.matrix-svc.loadbalancer.server.port=8090"

# Media Management
  lidarr:
    image: linuxserver/lidarr:latest
    container_name: lidarr
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    ports:
      - "$LIDARR_PORT:8686"
    volumes:
      - $USERDIR/docker/lidarr:/config
      - $USERDIR/Downloads:/downloads
      - $USERDIR/docker/media/music:/music
      - "/etc/localtime:/etc/localtime:ro"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers Auth Bypass
      - "traefik.http.routers.lidarr-rtr-bypass.entrypoints=https"
      - "traefik.http.routers.lidarr-rtr-bypass.rule=Headers(`X-Api-Key`, `$LIDARR_API_KEY`) || Query(`apikey`, `$LIDARR_API_KEY`)"
      - "traefik.http.routers.lidarr-rtr-bypass.priority=100"
      ## HTTP Routers Auth
      - "traefik.http.routers.lidarr-rtr.entrypoints=https"
      - "traefik.http.routers.lidarr-rtr.rule=Host(`lidarr.$DOMAINNAME`)"
      - "traefik.http.routers.lidarr-rtr.priority=99"
      ## Middlewares
      - "traefik.http.routers.lidarr-rtr-bypass.middlewares=chain-no-auth@file"
      - "traefik.http.routers.lidarr-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.lidarr-rtr.service=lidarr-svc"
      - "traefik.http.routers.lidarr-rtr-bypass.service=lidarr-svc"
      - "traefik.http.services.lidarr-svc.loadbalancer.server.port=8686"

  radarr:
    image: linuxserver/radarr
    container_name: radarr
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    ports:
      - "$RADARR_PORT:7878"
    volumes:
      - $USERDIR/docker/radarr:/config
      - $USERDIR/Downloads:/downloads
      - $USERDIR/docker/media/movies:/movies
      - "/etc/localtime:/etc/localtime:ro"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers Auth Bypass
      - "traefik.http.routers.radarr-rtr-bypass.entrypoints=https"
      - "traefik.http.routers.radarr-rtr-bypass.rule=Headers(`X-Api-Key`, `$RADARR_API_KEY`) || Query(`apikey`, `$RADARR_API_KEY`)"
      - "traefik.http.routers.radarr-rtr-bypass.priority=100"
      ## HTTP Routers Auth
      - "traefik.http.routers.radarr-rtr.entrypoints=https"
      - "traefik.http.routers.radarr-rtr.rule=Host(`radarr.$DOMAINNAME`)"
      - "traefik.http.routers.radarr-rtr.priority=99"
      ## Middlewares
      - "traefik.http.routers.radarr-rtr-bypass.middlewares=chain-no-auth@file"
      - "traefik.http.routers.radarr-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.radarr-rtr.service=radarr-svc"
      - "traefik.http.routers.radarr-rtr-bypass.service=radarr-svc"
      - "traefik.http.services.radarr-svc.loadbalancer.server.port=7878"

  radarr4k:
    image: linuxserver/radarr
    container_name: radarr4k
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    ports:
      - "$RADARR4K_PORT:7878"
    volumes:
      - $USERDIR/docker/radarr:/config
      - $USERDIR/Downloads:/downloads
      - $USERDIR/docker/media/movies4k:/movies
      - "/etc/localtime:/etc/localtime:ro"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers Auth Bypass
      - "traefik.http.routers.radarr-rtr-bypass.entrypoints=https"
      - "traefik.http.routers.radarr-rtr-bypass.rule=Headers(`X-Api-Key`, `$RADARR4K_API_KEY`) || Query(`apikey`, `$RADARR4K_API_KEY`)"
      - "traefik.http.routers.radarr-rtr-bypass.priority=100"
      ## HTTP Routers Auth
      - "traefik.http.routers.radarr-rtr.entrypoints=https"
      - "traefik.http.routers.radarr-rtr.rule=Host(`radarr4k.$DOMAINNAME`)"
      - "traefik.http.routers.radarr-rtr.priority=99"
      ## Middlewares
      - "traefik.http.routers.radarr-rtr-bypass.middlewares=chain-no-auth@file"
      - "traefik.http.routers.radarr-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.radarr-rtr.service=radarr-svc"
      - "traefik.http.routers.radarr-rtr-bypass.service=radarr-svc"
      - "traefik.http.services.radarr-svc.loadbalancer.server.port=7878"

  sonarr:
    image: linuxserver/sonarr
    container_name: sonarr
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    ports:
      - "$SONARR_PORT:8989"
    volumes:
      - $USERDIR/docker/sonarr:/config
      - $USERDIR/Downloads:/downloads
      - $USERDIR/docker/media/tv:/tv
      - "/etc/localtime:/etc/localtime:ro"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers Auth Bypass
      - "traefik.http.routers.sonarr-rtr-bypass.entrypoints=https"
      - "traefik.http.routers.sonarr-rtr-bypass.rule=Headers(`X-Api-Key`, `$SONARR_API_KEY`) || Query(`apikey`, `$SONARR_API_KEY`)"
      - "traefik.http.routers.sonarr-rtr-bypass.priority=100"
      ## HTTP Routers Auth
      - "traefik.http.routers.sonarr-rtr.entrypoints=https"
      - "traefik.http.routers.sonarr-rtr.rule=Host(`sonarr.$DOMAINNAME`)"
      - "traefik.http.routers.sonarr-rtr.priority=99"
      ## Middlewares
      - "traefik.http.routers.sonarr-rtr-bypass.middlewares=chain-no-auth@file"
      - "traefik.http.routers.sonarr-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.sonarr-rtr.service=sonarr-svc"
      - "traefik.http.routers.sonarr-rtr-bypass.service=sonarr-svc"
      - "traefik.http.services.sonarr-svc.loadbalancer.server.port=8989"

  sonarr4k:
    image: linuxserver/sonarr
    container_name: sonarr4k
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    ports:
      - "$SONARR_PORT:8989"
    volumes:
      - $USERDIR/docker/sonarr:/config
      - $USERDIR/Downloads:/downloads
      - $USERDIR/docker/media/tv4k:/tv4k
      - "/etc/localtime:/etc/localtime:ro"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers Auth Bypass
      - "traefik.http.routers.sonarr-rtr-bypass.entrypoints=https"
      - "traefik.http.routers.sonarr-rtr-bypass.rule=Headers(`X-Api-Key`, `$SONARR4K_API_KEY`) || Query(`apikey`, `$SONARR4K_API_KEY`)"
      - "traefik.http.routers.sonarr-rtr-bypass.priority=100"
      ## HTTP Routers Auth
      - "traefik.http.routers.sonarr-rtr.entrypoints=https"
      - "traefik.http.routers.sonarr-rtr.rule=Host(`sonarr4k.$DOMAINNAME`)"
      - "traefik.http.routers.sonarr-rtr.priority=99"
      ## Middlewares
      - "traefik.http.routers.sonarr-rtr-bypass.middlewares=chain-no-auth@file"
      - "traefik.http.routers.sonarr-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.sonarr-rtr.service=sonarr-svc"
      - "traefik.http.routers.sonarr-rtr-bypass.service=sonarr-svc"
      - "traefik.http.services.sonarr-svc.loadbalancer.server.port=8989"

  beets:
    image: linuxserver/beets
    container_name: beets
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=$TZ
    volumes:
      - $USERDIR/docker/beets/config:/config
      - $USERDIR/docker/media/music:/music
    restart: unless-stopped
    networks:
      - traefik
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.beets-rtr.entrypoints=https"
      - "traefik.http.routers.beets-rtr.rule=Host(`beets.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.beets-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.beets-rtr.service=beets-svc"
      - "traefik.http.services.beets-svc.loadbalancer.server.port=5800"

  jackett:
    image: linuxserver/jackett:latest
    container_name: jackett
    restart: always
    # network_mode: container:transmission-vpn
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      - $USERDIR/docker/jackett:/config
      - $USERDIR/Downloads:/downloads
      - "/etc/localtime:/etc/localtime:ro"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.jackett-rtr.entrypoints=https"
      - "traefik.http.routers.jackett-rtr.rule=Host(`jackett.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.jackett-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.jackett-rtr.service=jackett-svc"
      - "traefik.http.services.jackett-svc.loadbalancer.server.port=9117"

  hydra:
    image: linuxserver/nzbhydra2:latest
    container_name: nzbhydra2
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      - $USERDIR/docker/hydra2:/config
      - $USERDIR/Downloads:/downloads
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.hydra-rtr.entrypoints=https"
      - "traefik.http.routers.hydra-rtr.rule=Host(`hydra.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.hydra-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.hydra-rtr.service=hydra-svc"
      - "traefik.http.services.hydra-svc.loadbalancer.server.port=5076"

  filebot:
    image: jlesage/filebot:latest
    container_name: filebot
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      # A *.psm license file is required in /config
      # https://github.com/jlesage/docker-filebot#installing-a-license
      - $USERDIR/docker/filebot:/config
      - /$USERDIR/docker/media:/mediadir
      - $USERDIR/Downloads:/downloads
    environment:
      USER_ID: $PUID
      GROUP_ID: $PGID
      TZ: $TZ
      UMASK: 002
      KEEP_APP_RUNNING: 1
      CLEAN_TMP_DIR: 1
      DISPLAY_WIDTH: 1600
      DISPLAY_HEIGHT: 960
      VNC_PASSWD: $FILEBOT_VNC_PASSWD
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.filebot-rtr.entrypoints=https"
      - "traefik.http.routers.filebot-rtr.rule=Host(`filebot.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.filebot-rtr.middlewares=chain-authelia@file"
      ## HTTP Services
      - "traefik.http.routers.filebot-rtr.service=filebot-svc"
      - "traefik.http.services.filebot-svc.loadbalancer.server.port=5800"

  tvheadend:
    image: linuxserver/tvheadend
    container_name: tvheadend
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=$TZ
      #- RUN_OPTS=run options here
    volumes:
      - $USERDIR/docker/tvheadend:/config
    ports:
      - 9982:9982 #port for internal connections, not the webui
    networks:
      - traefik
    restart: unless-stopped
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.tvheadend-rtr.entrypoints=https"
      - "traefik.http.routers.tvheadend-rtr.rule=Host(`tvheadend.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.tvheadend-rtr.middlewares=chain-no-auth@file"
      ## HTTP Services
      - "traefik.http.routers.tvheadend-rtr.service=tvheadend-svc"
      - "traefik.http.services.tvheadend-svc.loadbalancer.server.port=9981"

# Frontend services
  ombi:
    image: linuxserver/ombi:latest
    container_name: ombi
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      - $USERDIR/docker/ombi:/config
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.ombi-rtr.entrypoints=https"
      - "traefik.http.routers.ombi-rtr.rule=Host(`requests.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.ombi-rtr.middlewares=chain-no-auth@file"
      ## HTTP Services
      - "traefik.http.routers.ombi-rtr.service=ombi-svc"
      - "traefik.http.services.ombi-svc.loadbalancer.server.port=3579"

  statping:
    image: hunterlong/statping:latest
    container_name: statping
    restart: unless-stopped
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      - $USERDIR/docker/statping:/app
    environment:
      VIRTUAL_HOST: localhost
      VIRTUAL_PORT: 8080
      DB_CONN: postgres
      DB_HOST: postgres
      DB_USER: $STATPING_DB_USER
      DB_PASS: $STATPING_DB_PASS
      DB_DATABASE: $STATPING_DB
      IS_DOCKER: "true"
      DISABLE_LOGS: "false"
      NAME: StatPing
      DESCRIPTION: Monitor web services
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.statping-rtr.entrypoints=https"
      - "traefik.http.routers.statping-rtr.rule=Host(`status.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.statping-rtr.middlewares=chain-no-auth@file"
      ## HTTP Services
      - "traefik.http.routers.statping-rtr.service=statping-svc"
      - "traefik.http.services.statping-svc.loadbalancer.server.port=8080"

  riot:
    ## Create default config:
    # docker run --rm --entrypoint cat bubuntux/riot-web /etc/riot-web/config.json > $USERDIR/docker/riot/config.json
    container_name: riot
    image: bubuntux/riot-web:latest
    restart: always
    networks:
      - traefik
    security_opt:
      - no-new-privileges:true
    volumes:
      - $USERDIR/docker/riot/config.json:/etc/riot-web/config.json:ro
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.riot-rtr.entrypoints=https"
      - "traefik.http.routers.riot-rtr.rule=Host(`riot.$DOMAINNAME`)"
      ## Middlewares
      - "traefik.http.routers.riot-rtr.middlewares=secure-chain@file"
      ## HTTP Services
      - "traefik.http.routers.riot-rtr.service=riot-svc"
      - "traefik.http.services.riot-svc.loadbalancer.server.port=80"

  ols-whmcs:
    #clone this into the docker folder before starting: https://github.com/litespeedtech/ols-docker-env
    #download whmcs and place in /sites | https://download.whmcs.com/
    image: litespeedtech/openlitespeed:latest
    env_file:
      - .env
    volumes:
        - $USERDIR/docker/ols-whmcs/lsws/conf:/usr/local/lsws/conf
        - $USERDIR/docker/ols-whmcs/lsws/admin-conf:/usr/local/lsws/admin/conf
        - $USERDIR/docker/ols-whmcs/bin/container:/usr/local/bin
        - $USERDIR/docker/ols-whmcs/sites:/var/www/vhosts/
        - $USERDIR/docker/ols-whmcs/acme:/root/.acme.sh/
        - $USERDIR/docker/ols-whmcs/logs:/usr/local/lsws/logs/
    restart: always
    environment:
      TZ: $TZ
    networks:
      - traefik
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.ols-whmcs-rtr.entrypoints=https"
      - "traefik.http.routers.ols-whmcs-rtr.rule=Host(`client.$DOMAINNAME`)"
      - "traefik.http.routers.ols-whmcs-rtr.tls=true"
      ## Middlewares
      - "traefik.http.routers.ols-whmcs-rtr.middlewares=chain-oauth@file,add-ols-whmcs"
      - "traefik.http.middlewares.add-ols-whmcs.addPrefix.prefix=/ols-whmcs"
      ## HTTP Services
      - "traefik.http.routers.ols-whmcs-rtr.service=ols-whmcs-svc"
      - "traefik.http.services.ols-whmcs-svc.loadbalancer.server.port=8080"

  ols-grav:
    #clone this into the docker folder before starting: https://github.com/litespeedtech/ols-docker-env
    #download grav and place it in ./sites | https://getgrav.org/download/core/grav-admin/1.6.26
    image: litespeedtech/openlitespeed:latest
    env_file:
      - .env
    volumes:
        - $USERDIR/docker/ols-grav/lsws/conf:/usr/local/lsws/conf
        - $USERDIR/docker/ols-grav/lsws/admin-conf:/usr/local/lsws/admin/conf
        - $USERDIR/docker/ols-grav/bin/container:/usr/local/bin
        - $USERDIR/docker/ols-grav/sites:/var/www/vhosts/
        - $USERDIR/docker/ols-grav/acme:/root/.acme.sh/
        - $USERDIR/docker/ols-grav/logs:/usr/local/lsws/logs/
    networks:
      - traefik
    restart: always
    environment:
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.ols-grav-rtr.entrypoints=https"
      - "traefik.http.routers.ols-grav-rtr.rule=Host(`$DOMAINNAME`)"
      - "traefik.http.routers.ols-grav-rtr.tls=true"
      ## Middlewares
      - "traefik.http.routers.ols-grav-rtr.middlewares=chain-oauth@file,add-ols-grav"
      - "traefik.http.middlewares.add-ols-grav.addPrefix.prefix=/ols-grav"
      ## HTTP Services
      - "traefik.http.routers.ols-grav-rtr.service=ols-grav-svc"
      - "traefik.http.services.ols-grav-svc.loadbalancer.server.port=8080"

  ols-limesurvey:
    #clone this into the docker folder before starting: https://github.com/litespeedtech/ols-docker-env
    #download limesurvey and place it in /sites | https://www.limesurvey.org/stable-release?download=3025:limesurvey428%20200608targz
    image: litespeedtech/openlitespeed:latest
    env_file:
      - .env
    volumes:
        - $USERDIR/docker/ols-limesurvey/lsws/conf:/usr/local/lsws/conf
        - $USERDIR/docker/ols-limesurvey/lsws/admin-conf:/usr/local/lsws/admin/conf
        - $USERDIR/docker/ols-limesurvey/bin/container:/usr/local/bin
        - $USERDIR/docker/ols-limesurvey/sites:/var/www/vhosts/
        - $USERDIR/docker/ols-limesurvey/acme:/root/.acme.sh/
        - $USERDIR/docker/ols-limesurvey/logs:/usr/local/lsws/logs/
    networks:
      - traefik
    restart: always
    environment:
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.ols-limesurvey-rtr.entrypoints=https"
      - "traefik.http.routers.ols-limesurvey-rtr.rule=Host(`$DOMAINNAME`)"
      - "traefik.http.routers.ols-limesurvey-rtr.tls=true"
      ## Middlewares
      - "traefik.http.routers.ols-limesurvey-rtr.middlewares=chain-oauth@file,add-ols-limesurvey"
      - "traefik.http.middlewares.add-ols-limesurvey.addPrefix.prefix=/ols-limesurvey"
      ## HTTP Services
      - "traefik.http.routers.ols-limesurvey-rtr.service=ols-limesurvey-svc"
      - "traefik.http.services.ols-limesurvey-svc.loadbalancer.server.port=8080"

  librespeed:
    #skip authentication and not sure it will work
    image: linuxserver/librespeed
    container_name: librespeed
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
    volumes:
      - $USERDIR/docker/librespeed:/config
    restart: unless-stopped
    networks:
      - traefik
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.librespeed-rtr.entrypoints=https"
      - "traefik.http.routers.librespeed-rtr.rule=Host(`speedtest.$DOMAINNAME`)"
      - "traefik.http.routers.librespeed-rtr.tls=true"
      ## Middlewares
      - "traefik.http.middlewares.add-librespeed.addPrefix.prefix=/librespeed"
      ## HTTP Services
      - "traefik.http.routers.librespeed-rtr.service=librespeed-svc"
      - "traefik.http.services.librespeed-svc.loadbalancer.server.port=8080"

# File System Mounts (will probably do on the system level or create a cache for each one and make them depend on each other)
  rclonemount1:
    # When you force remove the container, you have to sudo fusermount -u -z /mnt/mediaefs on the hostsystem!
    ##For filebot
    container_name: rclonemount1
    image: mumiehub/rclone-mount:latest
    restart: always
    privileged: true
    cap_add:
    - SYS_ADMIN
    devices:
    - /dev/fuse
    security_opt:
      - apparmor:unconfined
    stdin_open: true
    tty: true
    volumes:
      - $USERDIR/.config/rclone1:/config
      - /$USERDIR/docker/media:/mnt/gmedia:shared
    environment:
      - "ConfigDir=/config"
      - "ConfigName=rclone1.conf"
      - "RemotePath=gmedia:"
      - "MountPoint=/$USERDIR/docker/media/"
      - "MountCommands=--uid 1000 --gid 1000 --umask 022 --default-permissions --allow-other --allow-non-empty --cache-db-purge --dir-cache-time 72h --poll-interval 5m --buffer-size 128M"
      - "UnmountCommands=-u -z"

  rclonemount2:
    container_name: rclonemount2
    image: mumiehub/rclone-mount:latest
    restart: always
    privileged: true
    cap_add:
    - SYS_ADMIN
    devices:
    - /dev/fuse
    security_opt:
      - apparmor:unconfined
    stdin_open: true
    tty: true
    volumes:
      - $USERDIR/.config/rclone2:/config
      - /$USERDIR/docker/media/movies:/mnt/gmedia:shared
    environment:
      - "ConfigDir=/config"
      - "ConfigName=rclone2.conf"
      - "RemotePath=gmedia:"
      - "MountPoint=/$USERDIR/docker/media/movies"
      - "MountCommands=--uid 1000 --gid 1000 --umask 022 --default-permissions --allow-other --allow-non-empty --cache-db-purge --dir-cache-time 72h --poll-interval 5m --buffer-size 128M"
      - "UnmountCommands=-u -z"

  rclonemount3:
    container_name: rclonemount3
    image: mumiehub/rclone-mount:latest
    restart: always
    privileged: true
    cap_add:
    - SYS_ADMIN
    devices:
    - /dev/fuse
    security_opt:
      - apparmor:unconfined
    stdin_open: true
    tty: true
    volumes:
      - $USERDIR/.config/rclone3:/config
      - /$USERDIR/docker/media/movies4k:/mnt/gmedia:shared
    environment:
      - "ConfigDir=/config"
      - "ConfigName=rclone3.conf"
      - "RemotePath=gmedia:"
      - "MountPoint=/$USERDIR/docker/media/movies4k"
      - "MountCommands=--uid 1000 --gid 1000 --umask 022 --default-permissions --allow-other --allow-non-empty --cache-db-purge --dir-cache-time 72h --poll-interval 5m --buffer-size 128M"
      - "UnmountCommands=-u -z"

  rclonemount4:
    container_name: rclonemount4
    image: mumiehub/rclone-mount:latest
    restart: always
    privileged: true
    cap_add:
    - SYS_ADMIN
    devices:
    - /dev/fuse
    security_opt:
      - apparmor:unconfined
    stdin_open: true
    tty: true
    volumes:
      - $USERDIR/.config/rclone4:/config
      - /$USERDIR/docker/media/tv:/mnt/gmedia:shared
    environment:
      - "ConfigDir=/config"
      - "ConfigName=rclone4.conf"
      - "RemotePath=gmedia:"
      - "MountPoint=/$USERDIR/docker/media/tv"
      - "MountCommands=--uid 1000 --gid 1000 --umask 022 --default-permissions --allow-other --allow-non-empty --cache-db-purge --dir-cache-time 72h --poll-interval 5m --buffer-size 128M"
      - "UnmountCommands=-u -z"

  rclonemount5:
    container_name: rclonemount5
    image: mumiehub/rclone-mount:latest
    restart: always
    privileged: true
    cap_add:
    - SYS_ADMIN
    devices:
    - /dev/fuse
    security_opt:
      - apparmor:unconfined
    stdin_open: true
    tty: true
    volumes:
      - $USERDIR/.config/rclone5:/config
      - /$USERDIR/docker/media/tv:/mnt/gmedia:shared
    environment:
      - "ConfigDir=/config"
      - "ConfigName=rclone5.conf"
      - "RemotePath=gmedia:"
      - "MountPoint=/$USERDIR/docker/media/tv4k"
      - "MountCommands=--uid 1000 --gid 1000 --umask 022 --default-permissions --allow-other --allow-non-empty --cache-db-purge --dir-cache-time 72h --poll-interval 5m --buffer-size 128M"
      - "UnmountCommands=-u -z"

  rclonemount6:
    container_name: rclonemount6
    image: mumiehub/rclone-mount:latest
    restart: always
    privileged: true
    cap_add:
    - SYS_ADMIN
    devices:
    - /dev/fuse
    security_opt:
      - apparmor:unconfined
    stdin_open: true
    tty: true
    volumes:
      - $USERDIR/.config/rclone6:/config
      - /$USERDIR/docker/media/tv:/mnt/gmedia:shared
    environment:
      - "ConfigDir=/config"
      - "ConfigName=rclone6.conf"
      - "RemotePath=gmedia:"
      - "MountPoint=/$USERDIR/docker/media/music"
      - "MountCommands=--uid 1000 --gid 1000 --umask 022 --default-permissions --allow-other --allow-non-empty --cache-db-purge --dir-cache-time 72h --poll-interval 5m --buffer-size 128M"
      - "UnmountCommands=-u -z"

networks:
  traefik:
    external:
      name: traefik
